//
//  main.m
//  Cissi
//
//  Created by Cragnarsson on 2016-01-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
